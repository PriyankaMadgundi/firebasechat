package e.ssg0066.myapplication.constant;

public class URLConstants {
    public static final String FIREBASE_BASE_URL = "https://fcm.googleapis.com/fcm/send";

    public static final String GET_USERS_URL = FIREBASE_BASE_URL + "Users";
    public static final String GET_USER_URL = FIREBASE_BASE_URL + "Users/";

    public static final String GET_MESSAGES_URL = FIREBASE_BASE_URL + "messages";

    public static final String GET_CURRENT_USERS_URL = FIREBASE_BASE_URL + "currentUsers";
    public static final String GET_CURRENT_USER_URL = FIREBASE_BASE_URL + "currentUsers/";

    public static final String BASE_URL = "https://myapplication-341b5-default-rtdb.firebaseio.com/";

    public static final String ServerKey = "AAAAmmoHM80:APA91bHvw-4FMbIaBH_VzzS--6_c2dFK_s8TXpk-PbrrauELuI5hKl8cCn4ABBHkGdmty8VDensGSpWrFEOm9INDePeoYu7eH4CTb5NKW4iMghbAnoby7-LScvb2LeUT19Zoxiqv5oW2";
}
