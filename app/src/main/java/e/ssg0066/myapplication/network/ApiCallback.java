package e.ssg0066.myapplication.network;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;

import androidx.annotation.NonNull;

public interface ApiCallback<T> {

    void onSuccess(List<T> success);

    void onFailure();

    @NonNull
    TypeReference<List<T>> getClassOfType();
}
