package e.ssg0066.myapplication.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import e.ssg0066.myapplication.mvp.model.BaseResponse;

public class VolleyUtility {


    public <T> void callAPI(Context context, ApiCallback<T> apiCallback) {
        String url = "https://utter-prod.s3.ap-south-1.amazonaws.com/public/chat.json";
        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {

                        try {
                            ObjectMapper mapper = new ObjectMapper();
//                            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
//                            mapper.configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, false);
//                            mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, false);

                            BaseResponse baseResponse = mapper.readValue(response.toString(), BaseResponse.class);
                            if (baseResponse.isSuccess()) {
                                List<T> parsedResponse = mapper.readValue(mapper
                                                .writeValueAsString(baseResponse.getPayload()),
                                        apiCallback.getClassOfType());

                                apiCallback.onSuccess(parsedResponse);
                            } else {
                                apiCallback.onFailure();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        apiCallback.onFailure();
                    }
                });
        VolleyRequestQueue.getInstance(context).add(jsonObjectRequest);
    }

}
