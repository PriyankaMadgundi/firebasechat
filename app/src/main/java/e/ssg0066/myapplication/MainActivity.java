package e.ssg0066.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import e.ssg0066.myapplication.mvp.view.ChatActivity;
import e.ssg0066.myapplication.mvp.view.RegisterActivity;
import e.ssg0066.myapplication.mvp.Presenter.get_chat.GetChatPresenter;
import e.ssg0066.myapplication.mvp.Presenter.get_chat.GetChatContractor;
import e.ssg0066.myapplication.mvp.model.Message;
import e.ssg0066.myapplication.mvp.view.real_chat.RealChatActivity;
import e.ssg0066.myapplication.network.VolleyUtility;
import e.ssg0066.myapplication.storage.SpUtils;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GetChatContractor.View {

    private Button btnGetChat;
    private Button btnLogin;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGetChat = findViewById(R.id.btn_get_data);
        btnLogin = findViewById(R.id.btn_Login);
        btnRegister = findViewById(R.id.btn_register);

        btnGetChat.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnGetChat) {
            getChat();
        } else if (v == btnLogin) {
            startActivity(new Intent(this, RealChatActivity.class));
        } else if (v == btnRegister) {
            startActivity(new Intent(this, RegisterActivity.class));
        }
    }

    private void getChat() {
        GetChatPresenter getChatPresenter = new GetChatPresenter();
        getChatPresenter.attachView(this);
        getChatPresenter.callAPI(getApplicationContext(), new VolleyUtility());
    }

    @Override
    public void onChatSuccess(List<Message> response) {
        Toast.makeText(getApplication(), "DataFetched", Toast.LENGTH_LONG).show();

        SpUtils.getInstance(getApplicationContext()).saveChat(response);

        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        startActivity(intent);
    }

    @Override
    public void onChatFailed() {

    }
}