package e.ssg0066.myapplication.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import e.ssg0066.myapplication.mvp.model.Message;
import e.ssg0066.myapplication.mvp.model.MessageTest;

public class SpUtils {

    public static final String LOG_TAG = "SpUtils";
    private static final String FILE_NAME = "Test_sh";
    private static SpUtils instance;
    private static SharedPreferences sharedpreferences;
    private static final String USER_NAME = "userName";
    private static final String USER_PASSWORD = "userPassword";

    public static SpUtils getInstance(Context context) {

        if (instance == null) {
            instance = new SpUtils();
            sharedpreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        }
        return instance;
    }

    private void setKeyVal(String key, String val) {
        sharedpreferences.edit().putString(key, val).apply();
    }

    private void setKeyVal(String key, boolean val) {
        sharedpreferences.edit().putBoolean(key, val).apply();
    }

    private void setKeyVal(String key, long val) {
        sharedpreferences.edit().putLong(key, val).apply();
    }

    public void saveChat(List<Message> list) {
        try {
            MessageTest messageTest = new MessageTest();
            messageTest.setList(list);
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(messageTest);
            setKeyVal("CHAT", jsonInString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Message> getChat() {
        try {
            List<Message> list;
            String str = sharedpreferences.getString("CHAT", "");
            ObjectMapper mapper = new ObjectMapper();
            MessageTest messageTest = mapper.readValue(str, MessageTest.class);
            return messageTest.getList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*****
     * Clear all locally saved data from device
     */
    public void clearAllSavedData() {
        if (sharedpreferences != null)
            sharedpreferences.edit().clear().apply();
    }
}
