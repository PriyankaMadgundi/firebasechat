package e.ssg0066.myapplication.mvp.Presenter.get_chat;

import android.content.Context;

import java.util.List;

import androidx.annotation.NonNull;
import com.fasterxml.jackson.core.type.TypeReference;

import e.ssg0066.myapplication.mvp.model.Message;
import e.ssg0066.myapplication.network.ApiCallback;
import e.ssg0066.myapplication.network.VolleyUtility;

public class GetChatPresenter implements GetChatContractor.Presenter {

    GetChatContractor.View view;

    @Override
    public void attachView(GetChatContractor.View view) {
        this.view = view;
    }

    @Override
    public void callAPI(Context context, VolleyUtility volleyUtility) {
        volleyUtility.callAPI(context, new ApiCallback<Message>() {
            @Override
            public void onSuccess(List<Message> success) {
                view.onChatSuccess(success);
            }

            @Override
            public void onFailure() {
                view.onChatFailed();
            }

            @NonNull
            @Override
            public TypeReference<List<Message>> getClassOfType() {
                return new TypeReference<List<Message>>() {};
            }
        });
    }
}
