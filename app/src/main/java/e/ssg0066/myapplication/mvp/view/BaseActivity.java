package e.ssg0066.myapplication.mvp.view;

import androidx.appcompat.app.AppCompatActivity;
import e.ssg0066.myapplication.mvp.Presenter.base.BaseView;

public class BaseActivity extends AppCompatActivity implements BaseView {

    @Override
    public void showLoadingView(int message) {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showValidationErrorMessage(String errorMessage) {

    }
}
