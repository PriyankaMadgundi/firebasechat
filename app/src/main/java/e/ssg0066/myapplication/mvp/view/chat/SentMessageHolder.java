package e.ssg0066.myapplication.mvp.view.chat;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import e.ssg0066.myapplication.R;
import e.ssg0066.myapplication.mvp.model.Message;

public class SentMessageHolder extends RecyclerView.ViewHolder {
    TextView timeText;
    TextView messageText;

    SentMessageHolder(View itemView) {
        super(itemView);

        messageText = (TextView) itemView.findViewById(R.id.tv_message);
        timeText = (TextView) itemView.findViewById(R.id.tv_timeStamp);
    }

    void bind(Message message) {
        messageText.setText(message.getMessage());
    }
}
