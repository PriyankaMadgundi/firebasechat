package e.ssg0066.myapplication.mvp.model;

import java.util.ArrayList;

public class BaseResponse {
    private boolean success;
    private ArrayList<Object> payload;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<Object> getPayload() {
        return payload;
    }

    public void setPayload(ArrayList<Object> payload) {
        this.payload = payload;
    }


}
