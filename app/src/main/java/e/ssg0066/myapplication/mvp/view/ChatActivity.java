package e.ssg0066.myapplication.mvp.view;

import android.os.Bundle;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import e.ssg0066.myapplication.R;
import e.ssg0066.myapplication.mvp.model.Message;
import e.ssg0066.myapplication.mvp.view.chat.MessageListAdapter;
import e.ssg0066.myapplication.storage.SpUtils;

public class ChatActivity extends BaseActivity {

    private RecyclerView rvChat;
    private List<Message> list;
    private List<Message> messageList;
    private Handler threeSecHandler;
    private static final int MILLIS_SECOND_DELAY = 3000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);

        rvChat = findViewById(R.id.rv_chat);
        list = SpUtils.getInstance(getApplicationContext()).getChat();

        if (list != null && list.size() > 0) {
            messageList = new ArrayList<Message>();
            messageList.add(list.get(0));

            rvChat.setLayoutManager(new LinearLayoutManager(this));
            rvChat.setAdapter(new MessageListAdapter(ChatActivity.this, messageList));

            startTimer();
        }

    }

    private void startTimer() {
        if (threeSecHandler == null) {
            threeSecHandler = new Handler();
        } else {
            threeSecHandler.removeCallbacks(threeSecRunnable);
        }

        int time = MILLIS_SECOND_DELAY;
        threeSecHandler.postDelayed(threeSecRunnable, time);
    }

    private Runnable threeSecRunnable = new Runnable() {
        @Override
        public void run() {

            if (messageList.size() == list.size()) {
                threeSecHandler.removeCallbacks(this);
            } else {
                threeSecHandler.postDelayed(this, MILLIS_SECOND_DELAY);

                messageList.add(list.get(messageList.size()));
                rvChat.getAdapter().notifyItemInserted(messageList.size() - 1);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rvChat.smoothScrollToPosition(rvChat.getBottom());
                    }
                }, MILLIS_SECOND_DELAY);

            }

        }
    };
}
