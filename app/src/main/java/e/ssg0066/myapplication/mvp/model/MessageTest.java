package e.ssg0066.myapplication.mvp.model;

import java.util.List;

public class MessageTest {

    private List<Message> list;

    public List<Message> getList() {
        return list;
    }

    public void setList(List<Message> list) {
        this.list = list;
    }
}
