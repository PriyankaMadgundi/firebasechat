package e.ssg0066.myapplication.mvp.Presenter.base;

import e.ssg0066.myapplication.storage.SpUtils;

public interface BasePresenter<T extends BaseView> {
    void attachView(T view);
    void attachSharedPreference(SpUtils spUtils);
}
