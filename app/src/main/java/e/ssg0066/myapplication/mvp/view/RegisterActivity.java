package e.ssg0066.myapplication.mvp.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import e.ssg0066.myapplication.R;
import e.ssg0066.myapplication.mvp.Presenter.register.RegisterUserContractor;
import e.ssg0066.myapplication.mvp.Presenter.register.RegisterUserPresenter;

public class RegisterActivity extends BaseActivity implements View.OnClickListener, RegisterUserContractor.View {

    private TextInputEditText etEmail;
    private TextInputEditText etPassword;
    private TextInputEditText etConfirmPassword;
    private AppCompatButton btnRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        etConfirmPassword = findViewById(R.id.et_confirm_password);

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        RegisterUserPresenter registerUserPresenter = new RegisterUserPresenter();
        registerUserPresenter.attachView(this);
        registerUserPresenter.registerUserAPICall(etEmail.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString());
    }

    @Override
    public void onRegisterUserSuccess() {
        Toast.makeText(this, R.string.register_success, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onRegisterUserFailed() {

    }
}
