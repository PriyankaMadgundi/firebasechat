package e.ssg0066.myapplication.mvp.Presenter.base;

public interface BaseView {
    void showLoadingView(int message);

    void hideLoadingView();

    void showValidationErrorMessage(String errorMessage);
}
