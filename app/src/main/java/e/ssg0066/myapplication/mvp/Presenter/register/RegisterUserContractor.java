package e.ssg0066.myapplication.mvp.Presenter.register;

import e.ssg0066.myapplication.mvp.Presenter.base.BasePresenter;
import e.ssg0066.myapplication.mvp.Presenter.base.BaseView;

public class RegisterUserContractor {
    public interface View extends BaseView {
        void onRegisterUserSuccess();
        void onRegisterUserFailed();
    }

    public interface Presenter extends BasePresenter<RegisterUserContractor.View> {
        void registerUserAPICall(String username, String email, String password);
    }
}
