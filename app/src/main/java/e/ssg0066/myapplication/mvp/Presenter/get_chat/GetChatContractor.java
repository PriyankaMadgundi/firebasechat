package e.ssg0066.myapplication.mvp.Presenter.get_chat;

import android.content.Context;

import java.util.List;

import e.ssg0066.myapplication.mvp.model.Message;
import e.ssg0066.myapplication.network.VolleyUtility;

public class GetChatContractor {
    public interface View {
        void onChatSuccess(List<Message> response);
        void onChatFailed();
    }

    public interface Presenter {
        void attachView(GetChatContractor.View view);
        void callAPI(Context context, VolleyUtility volleyUtility);
    }
}
