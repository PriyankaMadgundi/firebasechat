package e.ssg0066.myapplication.mvp.Presenter.register;

import java.util.HashMap;
import java.util.Map;

import e.ssg0066.myapplication.storage.SpUtils;

public class RegisterUserPresenter implements RegisterUserContractor.Presenter {

    private RegisterUserContractor.View view;

    @Override
    public void attachView(RegisterUserContractor.View view) {
        this.view = view;
    }

    @Override
    public void attachSharedPreference(SpUtils spUtils) {

    }

    @Override
    public void registerUserAPICall(String username, String email, String password) {
//        new Firebase(URLConstants.GET_USER_URL)
//                .createUser(email, password, new Firebase.ValueResultHandler<Map<String, Object>>() {
//                    @Override
//                    public void onSuccess(Map<String, Object> stringObjectMap) {
//                        String uid = stringObjectMap.get("uid").toString();
//                        userRef = new Firebase(URLConstants.FirebaseURL + "Users/" + uid);
//                        userRef.setValue(createUser(username));
//                        view.onRegisterUserSuccess();
//                    }
//
//                    @Override
//                    public void onError(FirebaseError firebaseError) {
//                        view.onRegisterUserFailed();
//                    }
//                });
    }

    private Map<String, Object> createUser(String username) {
        Map<String, Object> user = new HashMap<>();
        user.put("username", username);
        return user;
    }
}
